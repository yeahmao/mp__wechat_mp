# -*- coding: utf-8 -*-
"""
@version: python3.6
@author: "Roger Lee"
@license: MIT Licence 
@contact: 704480843@qq.com
@file: utils.py
@time: 2018/9/8 11:16
"""

import hashlib
import time

from bs4 import BeautifulSoup


HOST = 'mp.weixin.qq.com'
BASEURL = 'https://' + HOST

URLS = {
  'home': BASEURL + '/cgi-bin/home',
  'masssendpage': BASEURL + '/cgi-bin/masssendpage',
  'bizlogin': BASEURL + '/cgi-bin/bizlogin',
  'loginqrcode': BASEURL + '/cgi-bin/loginqrcode',
  'operate_appmsg': BASEURL + '/cgi-bin/operate_appmsg',
  'appmsg': BASEURL + '/cgi-bin/appmsg',
  'filetransfer': BASEURL + '/cgi-bin/filetransfer',
  'filepage': BASEURL + '/cgi-bin/filepage',
  'masssend': BASEURL + '/cgi-bin/masssend',
  'safeassistant': BASEURL + '/misc/safeassistant',
  'safeqrconnect': BASEURL + '/safe/safeqrconnect',
  'safeqrcode': BASEURL + '/safe/safeqrcode',
  'safeuuid': BASEURL + '/safe/safeuuid',
  'singlesend': BASEURL + '/cgi-bin/singlesend',
  'message': BASEURL + '/cgi-bin/message',
  'uploadimg2cdn': BASEURL + '/cgi-bin/uploadimg2cdn',
  'verifycode': BASEURL + '/cgi-bin/verifycode',
  'userlist': BASEURL + '/cgi-bin/user_tag',
}


def encrypt(text):
    m = hashlib.md5()
    m.update(text)
    return m.hexdigest()


def from_timestamp_to_datetime_string(timestamp):
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(timestamp))


def parse_html(response):
    # 读取返回的内容，编码使用utf-8，放入变量html
    html = response.content.decode('utf-8')

    # 生成一个BeautifulSoup对象并放入变量soup
    soup = BeautifulSoup(html, 'lxml')

    # 返回BeautifulSoup对象
    return soup


def my_test():
    print('2021-8-3 22:20:46')

