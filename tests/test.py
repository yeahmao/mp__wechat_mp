# -*- coding: utf-8 -*-
"""
@version: python3.6
@author: "Roger Lee"
@license: MIT Licence 
@contact: 704480843@qq.com
@file: test.py.py
@time: 2018/9/8 11:43
"""
import json
import os
import sys
import requests


# 开发过程中，增加上级目录到path中
sys.path.insert(0, '..')
print(sys.path)


def foo_requests():
    requests.sessions.Session.post()


def _wechatData(token):
    from wechat_mp.utils import URLS
    url = '{}?t=mass/send&token={}'.format(URLS['masssendpage'], token)
    response = requests.get(url)
    print(response.text)


def 文章发布到草稿箱(we, msg):
    ret, appMsgId = we.operate_appmsg_create_caogao(msg)
    print(ret, appMsgId)


def 文章发布到图文素材(we, msg):
    ret, appMsgId = we.operate_appmsg_create_tuwen(msg)
    print(ret, appMsgId)


def main():
    # token = '863306660'
    # return _wechatData(token)
    # return foo_requests()

    from wechat_mp import WeChat, utils
    utils.my_test()
    rf = open(r'D:\mp_appmsg.json', 'r', encoding='utf-8')
    msg = json.loads(rf.read())

    # 2021-8-6
    # ticket_id: nine_cents
    # ticket: 5957d90e99471454453064803b116f316c842316
    # ck = json.loads(r'[{"expirationDate":2147483647,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"ua_id","path":"/","sameSite":"unspecified","value":"eh3g8oxZNFONpW7aAAAAAA4UGwomEfQZQ8vQZZL8-s0=","session":false,"hostOnly":true},{"expirationDate":2147454857.491942,"domain":"mp.weixin.qq.com","httpOnly":false,"secure":false,"name":"wxuin","path":"/","sameSite":"unspecified","value":"22862359075202","session":false,"hostOnly":true},{"expirationDate":1630458373,"domain":"mp.weixin.qq.com","httpOnly":false,"secure":false,"name":"noticeLoginFlag","path":"/","sameSite":"unspecified","value":"1","session":false,"hostOnly":true},{"expirationDate":1630458373,"domain":"mp.weixin.qq.com","httpOnly":false,"secure":false,"name":"remember_acct","path":"/","sameSite":"unspecified","value":"2075756470%40qq.com","session":false,"hostOnly":true},{"expirationDate":4294967295,"domain":"mp.weixin.qq.com","httpOnly":false,"secure":true,"name":"mm_lang","path":"/","sameSite":"unspecified","value":"zh_CN","session":false,"hostOnly":true},{"expirationDate":1628597966,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"rand_info","path":"/","sameSite":"unspecified","value":"CAESINE1g+0qDUpodGV/Kssq4N2Px3kcP3ptdK9AqJfJzW9d","session":false,"hostOnly":true},{"expirationDate":1628597966,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"slave_bizuin","path":"/","sameSite":"unspecified","value":"3014681641","session":false,"hostOnly":true},{"expirationDate":1628597966,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"data_bizuin","path":"/","sameSite":"unspecified","value":"3201461299","session":false,"hostOnly":true},{"expirationDate":1628597966,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"bizuin","path":"/","sameSite":"unspecified","value":"3014681641","session":false,"hostOnly":true},{"expirationDate":1628597966,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"data_ticket","path":"/","sameSite":"unspecified","value":"4Dj1jNHGyOMbYmpVShGFaEaB7rfdiVtnKCCMAlrm/at2rPLkRfaEC9TEewhns6uF","session":false,"hostOnly":true},{"expirationDate":1628597966,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"slave_sid","path":"/","sameSite":"unspecified","value":"TEpIN1AzMlNiYWhxUmhZOHI0RWlMcUpHVnNVMmNFR1JvQWVsTzg1aWxiMHpQazVoUExPU3YzVGFtUlBGOXQ2WTBRTlJqYkM5RmFvYTNOYm9abmVIUTRhUGZtbE9jeWY5TGhLM1pFQWhLQldXeXk4MzhBTmp6bkVDdURJZ3R2cndsSkR3RTRBVUtFNXM3Umt2","session":false,"hostOnly":true},{"expirationDate":1628597966,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"slave_user","path":"/","sameSite":"unspecified","value":"gh_65cf36895c01","session":false,"hostOnly":true},{"expirationDate":2147483647,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"xid","path":"/","sameSite":"unspecified","value":"dc877cc42a7432de33e6d6c4eb8119df","session":false,"hostOnly":true},{"expirationDate":1630725584,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"openid2ticket_oM2zSvjbbAMbavjNbemYjxZR-i1I","path":"/","sameSite":"unspecified","value":"iZd4AXfO0EY6uFXUUFK3hsobVwPJfmZTz0y1+wQr0Rw=","session":false,"hostOnly":true},{"domain":"mp.weixin.qq.com","httpOnly":false,"secure":false,"name":"rewardsn","path":"/","sameSite":"unspecified","value":"","session":true,"hostOnly":true},{"domain":"mp.weixin.qq.com","httpOnly":true,"secure":false,"name":"wxtokenkey","path":"/","sameSite":"unspecified","value":"777","session":true,"hostOnly":true}]')
    ck = json.loads(r'[{"expirationDate":2147483647,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"ua_id","path":"/","sameSite":"unspecified","value":"jXnSDG0OyLkLrQE2AAAAAGJ5jDyZinunFCT039FmztM=","session":false,"hostOnly":true},{"expirationDate":2147454843.473395,"domain":"mp.weixin.qq.com","httpOnly":false,"secure":false,"name":"wxuin","path":"/","sameSite":"unspecified","value":"23664778394335","session":false,"hostOnly":true},{"expirationDate":1629798384,"domain":"mp.weixin.qq.com","httpOnly":false,"secure":false,"name":"noticeLoginFlag","path":"/","sameSite":"unspecified","value":"1","session":false,"hostOnly":true},{"expirationDate":1629798384,"domain":"mp.weixin.qq.com","httpOnly":false,"secure":false,"name":"remember_acct","path":"/","sameSite":"unspecified","value":"2075756470%40qq.com","session":false,"hostOnly":true},{"expirationDate":4294967295,"domain":"mp.weixin.qq.com","httpOnly":false,"secure":true,"name":"mm_lang","path":"/","sameSite":"unspecified","value":"zh_CN","session":false,"hostOnly":true},{"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"uuid","path":"/","sameSite":"unspecified","value":"06bfb91c9e482bc84c5be074b0245457","session":true,"hostOnly":true},{"expirationDate":1628696146,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"rand_info","path":"/","sameSite":"unspecified","value":"CAESIIDAxwxXmbCXch8vtuyo1aRuqLb54Y5L2nqkeTSo6Rb4","session":false,"hostOnly":true},{"expirationDate":1628696146,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"slave_bizuin","path":"/","sameSite":"unspecified","value":"3014681641","session":false,"hostOnly":true},{"expirationDate":1628696146,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"data_bizuin","path":"/","sameSite":"unspecified","value":"3201461299","session":false,"hostOnly":true},{"expirationDate":1628696146,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"bizuin","path":"/","sameSite":"unspecified","value":"3014681641","session":false,"hostOnly":true},{"expirationDate":1628696146,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"data_ticket","path":"/","sameSite":"unspecified","value":"1SAgjubIVxT1W29No5lfbZhmlRo5GaHMBQ3F+3Mvf8Qrc2fRXJg8v694piTieTAv","session":false,"hostOnly":true},{"expirationDate":1628696146,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"slave_sid","path":"/","sameSite":"unspecified","value":"OWNHM2tVZktLa1RZc3ExSWFFckhVYnVSbGpHYjBRcHhRem9oUF9ScFhHTmY4RnJfNk1fM1dRTzhTS3hIWG5EZ1RiSHVCUTJkS3RjdnlUR3FVdndSb01uR09WUjEzMjdNc3d5aHZ5R29tZlFWSEVnVzhBMjA1WEZWUlJ0WGFFREhHTDMzSU5pNHJlbTlMb3dV","session":false,"hostOnly":true},{"expirationDate":1628696146,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"slave_user","path":"/","sameSite":"unspecified","value":"gh_65cf36895c01","session":false,"hostOnly":true},{"expirationDate":2147483647,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"xid","path":"/","sameSite":"unspecified","value":"533b765354e40fe9db577f1531740947","session":false,"hostOnly":true},{"expirationDate":1630906452,"domain":"mp.weixin.qq.com","httpOnly":true,"secure":true,"name":"openid2ticket_oM2zSvjbbAMbavjNbemYjxZR-i1I","path":"/","sameSite":"unspecified","value":"tMkzTAVRhdFR7F1v96BZquc1inoOBNoNg5rb/he6yCA=","session":false,"hostOnly":true}]')
    wx_data = json.loads("{\"t\":\"1835904796\",\"ticket\":\"71ee55fd37e0ee6d6805655120179a20c60c8589\",\"lang\":\"zh_CN\",\"param\":\"&token=1835904796&lang=zh_CN\",\"uin\":\"3014681641\",\"uin_base64\":\"MzAxNDY4MTY0MQ==\",\"user_name\":\"nine_cents\",\"nick_name\":\"acg美女cosplay营地\",\"nick_name_decode\":\"acg美女cosplay营地\",\"time\":\"1628343305\",\"disable_head_box\":0,\"open_app_type\":16,\"serviceType\":1,\"realnameType\":0,\"isPersonVerify\":0}")
    # we = WeChat('acg美女cosplay营地', 'pwd', ck=ck, token=token, wx_data=wx_data)
    we = WeChat('acg美女cosplay营地', cache=True)
    # we = WeChat('四不优漫', cache=True)
    # we = WeChat('若比邻便利店', cache=True)
    return 文章发布到草稿箱(we, msg)
    # return 文章发布到图文素材(we, msg)

    # 原有的功能
    # if False:
    #     accounts = we.search_account("cosplay", limit=10)
    #     print(accounts)
    #     we._wechatData()
    #     templates = we.get_templates()
    #     print(templates)

    # 上传远程图片上传至cdn
    # we.uploadimg2cdn('https://imgconvert.csdnimg.cn/aHR0cHM6Ly9tbWJpei5xcGljLmNuL21tYml6X2pwZy85UmRMZHpVTDk4aWFpYzRQYkw4Uk1LVTEyTEkxVnlPZERLdlY4dEFFTkhveFU2RE9CaGFIY2ljNDRzZk56ZGVpY3pyVERZbjhVYk5LTjJaaDdTQWt6VmNhZXcvNjQw?x-oss-process=image/format,png')

    # 本地文件上传
    # fn = 'K:\\_ALL\\tujigu\\tujigu_20_30K\\20324---Ai_Saitou_斉藤愛_seifuku1_[Cosdoki]_写真集[80P]_图集谷\\2.jpg'
    # we.upload_material(fn)

    # 发布文章
    with open(r'D:\mp_appmsg.json', 'r', encoding='utf-8') as rf:
        we.operate_appmsg(json.loads(rf.read()))
    return

    w = WeChat('liningning1992@live.cn', 'WLHlnn1314', True)

    w.get_user_propery('2020-03-10','2020-03-10')


if __name__ == "__main__":
    print("------------------    Enter __main__    ------------------")

    print(u"[Current work directory is : ]\t" + os.getcwd())
    print(u"[Current process ID is : ]\t" + str(os.getpid()))
    print("\n")
    main()

    print("------------------    Leave __main__    ------------------")